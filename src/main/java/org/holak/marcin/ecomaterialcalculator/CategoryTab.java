package org.holak.marcin.ecomaterialcalculator;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryTab extends Tab {
    private String name;
    private List<Tab> childTabs;

    @FXML
    public TextField newSpecialtyNameTextField;

    @FXML
    public TabPane specialtyTabPane;

    public CategoryTab(String name, Map<String, String> parameters) {
        this.name = name;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/categoryTab.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }
    }

    public void addSpecialtyTab(MouseEvent event) {
        String name = newSpecialtyNameTextField.getText();
        newSpecialtyNameTextField.clear();
        SpecialtyTab newTab = new SpecialtyTab(name, this.name, new HashMap<>());
        newTab.setText(name);
        childTabs.add(newTab);
        List<SpecialtyTab> childrenSpecialties = Main.TABS.get(this);
        childrenSpecialties.add(newTab);
        specialtyTabPane.getTabs().add(specialtyTabPane.getTabs().size() - 1, newTab);
        specialtyTabPane.getSelectionModel().select(newTab);
    }

    public String getName() {
        return name;
    }
}

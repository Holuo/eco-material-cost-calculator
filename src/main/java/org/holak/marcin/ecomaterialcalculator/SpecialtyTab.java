package org.holak.marcin.ecomaterialcalculator;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;

import java.io.IOException;
import java.util.Map;

public class SpecialtyTab extends Tab {
    private String parentCategory;
    private String name;
    private int skillValue;
    private boolean workflowChoice;
    private boolean lavishWorkspaceChoice;
    private double multiplier;

    public SpecialtyTab(String name, String parentCategory, Map<String, String> parameters) {
        this.name = name;
        this.parentCategory = parentCategory;
        this.skillValue = 0;
        this.workflowChoice = false;
        this.lavishWorkspaceChoice = false;
        this.multiplier = 1;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/specialtyTab.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }
    }

    private void calculateMultiplier() {
        if (skillValue == 0) {
            multiplier = 1;
        } else {
            multiplier = 0.55 - 0.05 * skillValue;
            if (skillValue >= 6 && lavishWorkspaceChoice) {
                multiplier = multiplier * 0.9;
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSkillValue() {
        return skillValue;
    }

    public void setSkillValue(int skillValue) {
        this.skillValue = skillValue;
        calculateMultiplier();
    }

    public boolean isWorkflowChoice() {
        return workflowChoice;
    }

    public void setWorkflowChoice(boolean workflowChoice) {
        this.workflowChoice = workflowChoice;
    }

    public boolean isLavishWorkspaceChoice() {
        return lavishWorkspaceChoice;
    }

    public void setLavishWorkspaceChoice(boolean lavishWorkspaceChoice) {
        this.lavishWorkspaceChoice = lavishWorkspaceChoice;
        calculateMultiplier();
    }

    public double getMultiplier() {
        return multiplier;
    }
}

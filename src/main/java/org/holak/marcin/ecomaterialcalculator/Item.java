package org.holak.marcin.ecomaterialcalculator;

public class Item {
    private String name;
    private double cost;
    private boolean isSelfMade;
    private boolean isIngredient;

    public Item(String name, double cost, boolean isSelfMade, boolean isIngredient) {
        this.name = name;
        this.cost = cost;
        this.isSelfMade = isSelfMade;
        this.isIngredient = isIngredient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public boolean isSelfMade() {
        return isSelfMade;
    }

    public void setSelfMade(boolean selfMade) {
        this.isSelfMade = selfMade;
    }

    public boolean isIngredient() {
        return isIngredient;
    }

    public void setIngredient(boolean ingredient) {
        isIngredient = ingredient;
    }
}

package org.holak.marcin.ecomaterialcalculator;

import javafx.scene.control.TextArea;

import java.util.Map;

public class Recipe {
    private String name;
    private String specialty;
    private Item outputItem;
    private int craftTime;
    private double cost;
    private Map<Item, Double> ingredients;

    public Recipe(String name, String specialty, Item item, int time, Map<Item, Double> ingredients) {
        this.name = name;
        this.specialty = specialty;
        this.outputItem = item;
        this.craftTime = time;
        this.ingredients = ingredients;
    }

    public void calculateCost(double multiplier, TextArea textArea) {
        StringBuilder displayTextSB = new StringBuilder(name + ", producing: " + outputItem.getName() + "\n\n");
        cost = 0;
        for (Map.Entry<Item, Double> ingredient : ingredients.entrySet()) {
            String ingredientName = ingredient.getKey().getName();
            double ingredientCost = ingredient.getKey().getCost();
            double ingredientAmount = ingredient.getValue();
            double calculatedAmount = ingredientAmount * multiplier;
            double calculatedCost = calculatedAmount * ingredientCost;
            cost += calculatedCost;

            displayTextSB.append(ingredientName).append(" - ").append(calculatedAmount).append("\n");
        }
        displayTextSB.append("\nCost: ").append(cost);
        textArea.setText(displayTextSB.toString());
    }

    public double getCost() {
        return this.cost;
    }


}

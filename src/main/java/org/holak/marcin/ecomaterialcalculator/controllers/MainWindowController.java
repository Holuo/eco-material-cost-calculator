package org.holak.marcin.ecomaterialcalculator.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.holak.marcin.ecomaterialcalculator.CategoryTab;
import org.holak.marcin.ecomaterialcalculator.Main;
import org.holak.marcin.ecomaterialcalculator.SpecialtyTab;

import java.util.ArrayList;
import java.util.HashMap;

public class MainWindowController {

    @FXML
    public Tab addNewTab;

    @FXML
    public TextField newCategoryNameTextField;

    @FXML
    private TabPane categoryTabPane;

    public void initialize() {

    }

    public void addCategoryTab(MouseEvent event) {
        String name = newCategoryNameTextField.getText();
        newCategoryNameTextField.clear();
        CategoryTab newTab = new CategoryTab(name, new HashMap<>());
        newTab.setText(name);
        categoryTabPane.getTabs().add(categoryTabPane.getTabs().size() - 1, newTab);
        categoryTabPane.getSelectionModel().select(newTab);
        Main.TABS.put(newTab, new ArrayList<SpecialtyTab>());
    }
}
